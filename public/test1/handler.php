<?php

/* simple AJAX check  */
if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
    die('Ajax calls only please');
}

$errors = [];

// check required fields
$requiredFields = ['name', 'email', 'phone'];

foreach ($requiredFields as $requiredField) {

    if (!isset($_POST[$requiredField])) {

        $errors[] = "$requiredField is required";

    }

}

// validations
if (empty($errors)) {

    // we are not going to extract() the $_POST because it can contain more fields than we expect, like usernames or whatever
    $name = $_POST['name'];

    if (strlen($name) < 3) {
        // error messages avoid outputting the invalid field content to avoid XSS
        $errors[] = "Your name must be at least 3 characters long";
    }

    $email = $_POST['email'];

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = "That does not appear to be a valid email address";
    }

    $phone = $_POST['phone'];

    if (!preg_match('/^[0-9- ]+$/D', $phone)) {
        $errors[] = "Phone numbers may only contain digits, space, and hyphens";
    }

}

if (empty($errors)) {

    // do whatever we want with the data

    // send response
    $response = ["status" => "ok"];

} else {

    $response = ["status" => "error", "messages" => $errors];

}

header('Content-Type: application/json');
echo json_encode($response);