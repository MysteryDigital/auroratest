$(document).ready( function() {

    $('#contact_form').submit(function(e) {

        e.preventDefault();

        var payload = $("#contact_form").serialize();

        console.log(payload);

        $("#error_container").hide();
        $("#error_messages").empty();

        $.ajax({
            type: "POST",
            url: "handler.php",
            data: payload,
            dataType: "json",
            success: function(data) {
                if (data.status == "ok") {

                    alert("Okay, all done, we would do a nicer notification now");

                } else {
                    // loop through data.messages and add them to the error container
                    // moving on to the next question

                    $("#error_container").show();

                }
            },
            error: function() {
                alert('error handing here');
            }
        });

    });

});
