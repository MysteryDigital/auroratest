<?php

/**
 * Class Pipeline
 * This class is only available as a singleton
 * Reads an input file into a multi-dimensional array, flattens it, and outputs to a new file
 */
class Pipeline
{
    /**
     * Instance of self
     */
    private static $instance;

    /**
     * Pipeline constructor.
     * Constructor is private access to prevent outside classes from instantiating directly
     */
    private function __construct()
    {

    }

    /**
     * Converts the data to another format
     * @param array $data
     * @return string
     */
    public function convert(array $data): string
    {
        // If I had time I would ask for clarity on the spec as to what format is required,
        // the spec just says "another" format
        $convertedData = json_encode($data);

        return $convertedData;
    }

    /**
     * Accepts a multidimensional array and flattens it down to a single dimension
     * @param array $multiDimensional
     * @return array
     */
    public function flattenArray(array $multiDimensional): array
    {
        $flat = [];

        $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($multiDimensional));
        foreach($iterator as $key => $value) {
            $flat[$key] = $value;
        }

        return $flat;
    }

    /**
     * Factory to return a singleton instance
     * @return Pipeline
     */
    public static function getInstance()
    {
        if (self::$instance === null) {

            $class = get_called_class();

            self::$instance = new $class;

        }

        return self::$instance;
    }

    /**
     * Reads a file that is formatted to PHP ini standard and returns the data as an array
     * @param string $fileName
     * @return array
     * @throws RuntimeException
     */
    public function readFile(string $fileName): array
    {
        if (!$dataArray = parse_ini_file($fileName, true)) {
            throw new RuntimeException('Please specify a valid filename, if this is a valid file please check permissions');
        }

        return $dataArray;
    }

    /**
     * Writes a data string to a file
     * @param string $fileName
     * @param string $data
     * @throws RuntimeException;
     */
    public function writeFile(string $fileName, string $data)
    {
        // open and truncate file, move cursor to 0 position
        if (!$handle = fopen($fileName, "w")) {
            throw new RuntimeException('I could not open the file specified for writing, if this is a valid filename please check permissions');
        }

        if (fwrite($handle, $data) != strlen($data)) {
            throw new RuntimeException('I did not write out the expected number of bytes');
        }

        fclose($handle);
    }

}