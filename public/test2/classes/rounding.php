<?php

/**
 * Class Rounding
 * Rounds numerical values to closest integer value when converting before saving
 */
class Rounding extends pipeline
{
    /**
     * Instance of self
     */
    private static $instance;

    private function __construct()
    {

    }

    /**
     * Rounds numerical values to closest integer value, then uses the parent class
     * to convert it, so that we don't have to rewrite the conversion code
     * @param array $data
     * @return string
     */
    public function convert(array $data): string
    {
        // array is single dimensional at this point
        foreach ($data as $key => $value) {
            if (is_numeric($value)) {
                // I prefer not to use associative foreach loops
                $data[$key] = round($value, 0);
            }
        }

        $convertedData = parent::convert($data);

        return $convertedData;
    }

    /**
     * Factory to return a singleton instance
     * We have to declare this so that we reference the instance of the *new* class
     * @return Pipeline
     */
    public static function getInstance()
    {
        if (self::$instance === null) {

            self::$instance = new Rounding;

        }

        return self::$instance;
    }

}