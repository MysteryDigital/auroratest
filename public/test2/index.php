<?php

require('classes/pipeline.php');
require('classes/rounding.php');

// run it without the extension

try {

    $pipeline = Pipeline::getInstance();

    $data = $pipeline->readFile("example.ini");

    $flatData = $pipeline->flattenArray($data);

    $convertedData = $pipeline->convert($flatData);

    $pipeline->writeFile('output.json', $convertedData);

} catch (RuntimeException $e) {

    echo "Runtime exception : {$e->getMessage()}";

} catch (Exception $e) {

    echo "Unexpected exception : {$e->getMessage()}";

}

// run it with the modification

try {

    $pipeline = Rounding::getInstance();

    $data = $pipeline->readFile("example.ini");

    $flatData = $pipeline->flattenArray($data);

    $convertedData = $pipeline->convert($flatData);

    $pipeline->writeFile('output-rounded.json', $convertedData);

} catch (RuntimeException $e) {

    echo "Runtime exception : {$e->getMessage()}";

} catch (Exception $e) {

    echo "Unexpected exception : {$e->getMessage()}";

}

echo "Done, normal end of run";

